package com.hardiani_10191038.dashboardapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    RecyclerView rv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String [] text = new String[]{
                "Facebook","Google","Instagram","Pinterst", "TikTok", "Twitter", "WhatsApp", "Youtube"
        };

        int[] image=new int[]{
                R.drawable.facebook,
                R.drawable.google,
                R.drawable.instagram,
                R.drawable.pinterest,
                R.drawable.tiktok,
                R.drawable.twitter,
                R.drawable.whatsapp,
                R.drawable.youtube
        };

        rv = findViewById(R.id.recyclerview);
        RvAdapter adapter = new RvAdapter(this, text, image);
        rv.setAdapter(adapter);
        rv.setLayoutManager(new GridLayoutManager(this,2));
    }
}